def vowel_swapper(string):
    # ==============
    # Your code here
    first = 0
    n = 2
    replace = [("a", "4"),
               ("A", "4"),
               ("e", "3"),
               ("E", "3"),
               ("i", "!"),
               ("I", "!"),
               ("o", "ooo"),
               ("O", "000"),
               ("u", "|_|"),
               ("U", "|_|")]
    y = string.lower()
    x = string.lower().find("a")

    while x >= 0 and n > 1:
        x = string.find("a", x+len("a"))
        n -= 1
    print(x)

    for old, new in replace:
        string = string.replace(old, new)
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
