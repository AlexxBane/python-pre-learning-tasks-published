def factors(number):
    # ==============
    # Your code here
    list = []
    i = 1

    while i < number:
        ans = float(number / i)

        if number % i == 0:
            list.append(int(ans))
        i += 1


    list.pop(0)
    list.reverse()
    if not list:
        string = str(number)
        list = string + ' is not a prime number'

    return list
    # ==============


print(factors(15))  # Should print [3, 5] to the console
print(factors(12))  # Should print [2, 3, 4, 6] to the console
print(factors(13))  # Should print “13 is a prime number”
