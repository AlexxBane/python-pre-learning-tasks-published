def factors(number):
    # ==============
    # Your code here
    list = []
    i = 1


    while i < number:
        ans = float(number / i)

        if number % i == 0:
            list.append(int(ans))
        i += 1

    list.pop(0)
    list.reverse()


    return list

    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
